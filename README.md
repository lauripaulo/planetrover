# planetrover

Several rovers are in a strange planet, receiving commands from a far, far away base. The commands can make the rovers turn to a direction (East, West, North and South) and to move forward. The landscape is a square and each rover moves in order.

## Usage

Open a terminal and go to the place where you dowloaded this repo. Make sure you have Python 3 installed (any recent version will do). Where you have everything set just type: 

```bash
python rover.py <file-with-commands>
```
You and also call for help with: 

```bash
python rover.py -h
```

And the program will show this help.

```bash
usage: rover.py [-h] file

Planet rover simulator.

positional arguments:
  file        text file with commands.

optional arguments:
```

The simulator receives a file containing the field definition, rovers locations and the direction each is facing. The directions are North, South, East, West and the file must have the first direction letter (ex. S for South). Open the testcommands.txt file for a more detailed example.

## Definitions

### Movement

If the probe move to a coordinate outside the defined field it will report the last **valid** positions it had before leaving the field.

An example would be a 3,3 field with a probe at 0,0 heading south. When it moves it will go to 0,-1 that is invalid. When asked the probe will return the last valid coordinate that is 0,0.

### Probe colliding with another probe

No, the probes cannot collide with each other. They are rather small and the *Small Infinite Improbability Drive* each probe has manages to make them safe from each other.

## And one more thing

This is cool, a rover at your figertips to paly with! Give it a try. Have fun!

If you want to see another random projects make sure to visit [my Github repositories](https://github.com/lauripaulo). 

You can tweet me @lauripaulo.
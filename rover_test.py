#
# MIT License
#
# Copyright (c) 2020 Lauri Paulo Laux Jr
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import unittest

from rover import Rover, RoverCommander, Field, MissionControl
from rover import RIGHT, LEFT, MOVE, EAST, NORTH, SOUTH, WEST

class TestFieldMethods(unittest.TestCase):

    def test_create_field(self):
        field = Field(2, 3)
        self.assertEqual(field.xsize, 2)
        self.assertEqual(field.ysize, 3)
        
    def test_create_bad_field(self):
        with self.assertRaises(ValueError):
            Field (0, 0)
        with self.assertRaises(ValueError):
            Field (-1, 0)
        with self.assertRaises(ValueError):
            Field (-1, -1)
        with self.assertRaises(ValueError):
            Field (0, -1)


class TestRoverMethods(unittest.TestCase):

    def test_rover_rotate_right(self):
        field = Field(5,5)
        probe = Rover(NORTH, 0, 0, field)
        probe.rotate(RIGHT)
        self.assertEqual(probe.direction(), EAST)
        probe.rotate(RIGHT)
        self.assertEqual(probe.direction(), SOUTH)
        probe.rotate(RIGHT)
        self.assertEqual(probe.direction(), WEST)
        probe.rotate(RIGHT)
        self.assertEqual(probe.direction(), NORTH)

    def test_rover_rotate_left(self):
        field = Field(5,5)
        probe = Rover(NORTH, 0, 0, field)
        probe.rotate(LEFT)
        self.assertEqual(probe.direction(), WEST)
        probe.rotate(LEFT)
        self.assertEqual(probe.direction(), SOUTH)
        probe.rotate(LEFT)
        self.assertEqual(probe.direction(), EAST)
        probe.rotate(LEFT)
        self.assertEqual(probe.direction(), NORTH)

    def test_rover_basic_move(self):
        field = Field(5, 5)
        probe = Rover(NORTH, 1, 1, field)
        probe.move()
        self.assertEqual(probe.position(), {"x": 1, "y": 2})
        self.assertEqual(probe.direction(), NORTH)
        probe.rotate(RIGHT)
        probe.move()
        self.assertEqual(probe.position(), {"x": 2, "y": 2})
        self.assertEqual(probe.direction(), EAST)
        probe.rotate(RIGHT)
        probe.move()
        self.assertEqual(probe.position(), {"x": 2, "y": 1})
        self.assertEqual(probe.direction(), SOUTH)
        probe.rotate(RIGHT)
        probe.move()
        self.assertEqual(probe.position(), {"x": 1, "y": 1})
        self.assertEqual(probe.direction(), WEST)
        probe.rotate(LEFT)
        self.assertEqual(probe.direction(), SOUTH)
        probe.rotate(LEFT)
        self.assertEqual(probe.direction(), EAST)
        probe.move()
        self.assertEqual(probe.position(), {"x": 2, "y": 1})

    def test_rover_move_outside_field(self):
        field = Field(3, 3)
        probe = Rover(NORTH, 1, 1, field)
        probe.move()
        self.assertEqual(probe.position(), {"x": 1, "y": 2})
        probe.move()
        self.assertEqual(probe.position(), {"x": 1, "y": 3})
        probe.move()
        self.assertEqual(probe.position(), {"x": 1, "y": 3})
        self.assertFalse(probe.alive)
        probe.rotate(RIGHT)
        probe.move()
        self.assertEqual(probe.position(), {"x": 1, "y": 3})
        self.assertFalse(probe.alive)
        self.assertEqual(probe.direction(), NORTH)


class TestRoverCommanderMethods(unittest.TestCase):

    def test_basic_execute(self):
        field = Field(5, 5)
        probe = Rover(NORTH, 2, 2, field)
        commander = RoverCommander(probe, field)
        commander.execute("LM")
        self.assertEqual(probe.position(), {"x": 1, "y": 2})
        self.assertEqual(probe.direction(), WEST)
        self.assertTrue(probe.alive)

    def test_medium_execute(self):
        field = Field(5, 5)
        probe = Rover(NORTH, 2, 2, field)
        commander = RoverCommander(probe, field)
        commander.execute("LMLMLMLMRMRMRMRM")
        self.assertEqual(probe.position(), {"x": 2, "y": 2})
        self.assertEqual(probe.direction(), NORTH)
        self.assertTrue(probe.alive)

    def test_bad_execute(self):
        field = Field(2, 2)
        probe = Rover(NORTH, 1, 1, field)
        commander = RoverCommander(probe, field)
        commander.execute("LMLMLMLMRMRMRMRMMMMMMMMMMMM")
        self.assertEqual(probe.position(), {"x": 1, "y": 2})
        self.assertEqual(probe.direction(), NORTH)
        self.assertFalse(probe.alive)


class TestMissionControlMethods(unittest.TestCase):

    def test_basic_execute(self):
        commands = [
            "5 5",
            "1 2 N",
            "LMLMLMLMM",
            "3 3 E",
            "MMRMMRMRRM"
        ]
        expected_result = [
            "1 3 N",
            "5 1 E"
        ]
        control = MissionControl(commands)
        control.execute()
        mission_result = control.results()
        self.assertEqual(mission_result, expected_result)

    def test_bad_field_execute(self):
        commands = [
            "55",
            "1 2 N",
            "LMLMLMLMM"
        ]
        expected_result = []
        control = MissionControl(commands)
        with self.assertRaises(ValueError):
            control.execute()
        mission_result = control.results()
        self.assertEqual(mission_result, expected_result)

    def test_bad_probeinfo_execute(self):
        commands = [
            "5 5",
            "12 N",
            "LMLMLMLMM"
        ]
        expected_result = []
        control = MissionControl(commands)
        with self.assertRaises(ValueError):
            control.execute()
        mission_result = control.results()
        self.assertEqual(mission_result, expected_result)

    def test_bad_probecommands_execute(self):
        commands = [
            "5 5",
            "1 2 N",
            ""
        ]
        expected_result = []
        control = MissionControl(commands)
        with self.assertRaises(ValueError):
            control.execute()
        mission_result = control.results()
        self.assertEqual(mission_result, expected_result)

    def test_missing_probecommands_execute(self):
        commands = [
            "5 5",
            "1 2 N"
        ]
        expected_result = []
        control = MissionControl(commands)
        with self.assertRaises(ValueError):
            control.execute()
        mission_result = control.results()
        self.assertEqual(mission_result, expected_result)

    def test_bad_2nd_probe_execute(self):
        commands = [
            "5 5",
            "1 2 N",
            "LMLMLMLMM",
            "33 E",
            "MMRMMRMRRM"
        ]
        expected_result = [
            "1 3 N"
        ]
        control = MissionControl(commands)
        with self.assertRaises(ValueError):
            control.execute()
        mission_result = control.results()
        self.assertEqual(mission_result, expected_result)

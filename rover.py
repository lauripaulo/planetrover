#!/local/bin/python3

#
# MIT License
#
# Copyright (c) 2020 Lauri Paulo Laux Jr
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse

NORTH = "N"
SOUTH = "S"
EAST = "E"
WEST = "W"
LEFT = "L"
RIGHT = "R"
MOVE = "M"

class Field():

    def __init__(self, x, y):
        if x < 0 or y < 0:
            raise ValueError("Field x,y size must be positive or zero.")
        elif x == 0 and y == 0:
            raise ValueError("Field must have x or y size greater than zero.")
        self.xsize = x
        self.ysize = y
   

class Rover():

    def __init__(self, facing, x, y, field):
        self.compass = [NORTH, EAST, SOUTH, WEST]
        self.calcsheet = [
            {"x": 0, "y": 1}, 
            {"x": 1, "y": 0},
            {"x": 0, "y": -1},
            {"x": -1, "y": 0}
        ]
        self.facing = self.compass.index(facing)
        self.x = x
        self.y = y
        self.field = field
        self.alive = True

    def rotate(self, direction):
        if self.alive:
            if direction == LEFT:
                if self.facing == 0:
                    self.facing = len(self.compass) - 1
                else:
                    self.facing -= 1
            elif direction == RIGHT:
                if self.facing == len(self.compass) - 1:
                    self.facing = 0
                else:
                    self.facing += 1

    def direction(self):
        return self.compass[self.facing]
    
    def move(self):
        if self.alive:
            new_x = self.x + self.calcsheet[self.facing]["x"]
            new_y = self.y + self.calcsheet[self.facing]["y"]
            if new_x > self.field.xsize or new_x < 0:
                self.alive = False
            elif new_y > self.field.ysize or new_y < 0:
                self.alive = False
            else:
                self.x = new_x
                self.y = new_y
    
    def position(self):
        return {"x": self.x, "y": self.y}


class RoverCommander():

    def __init__(self, rover, field):
        self.rover = rover
        self.field = Field

    def execute(self, commands):
        for step in commands:
            if step == LEFT:
                self.rover.rotate(LEFT)
            elif step == RIGHT:
                self.rover.rotate(RIGHT)
            elif step == MOVE:
                self.rover.move()


class MissionControl() :

    def __init__(self, all_commands):
        self.all_commands = all_commands
        self.probe_positions = []

    def execute(self):
        field_cmd = self.all_commands.pop(0).split(" ")
        if len(field_cmd) != 2:
            raise ValueError ("Bad field size information.")
        field = Field(int(field_cmd[0]), int(field_cmd[1]))
        
        while len(self.all_commands) > 0:
            initpos = self.all_commands.pop(0).split(" ")
            if len(self.all_commands) == 0:
                raise ValueError ("Missing probe command line.")
            cmds = self.all_commands.pop(0)
            if len(initpos) != 3:
                raise ValueError ("Bad probe initial info.")
            if len(cmds) == 0:
                raise ValueError ("Bad probe command line.")
            probe = Rover(facing=initpos[2], 
                x=int(initpos[0]), y=int(initpos[1]), field=field)
            commander = RoverCommander(probe, field)
            commander.execute(cmds)
            posinfo = "{} {} {}".format(probe.x, probe.y, probe.direction())
            self.probe_positions.append(posinfo)

    def results(self):
        return self.probe_positions


def main():
    try:
        commands = []
        with open(args.file) as datafile:
            for line in datafile:
                commands.append(line.replace("\n", "").upper())
        control = MissionControl(commands)
        control.execute()
        for result in control.results():
            print("{}".format(result))
    except Exception as e:
        print(e)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Planet rover simulator - v0.1")
    parser.add_argument("file", metavar="file", type=str, 
        help="text file with commands.")
    args = parser.parse_args()
    if not args.file:
        parser.print_help()
    else:
        main()
